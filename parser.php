<?php
/**
 * Created by PhpStorm.
 * User: Denka
 * Date: 01.02.2019
 * Time: 21:48
 */
   require_once 'ParserInterface.php';

   /**
    * @author Victor Zinchenko <zinchenko.us@gmail.com>
    */

   class Parser implements ParserInterface
    {
        public function process(string $url, string $tag):array
            {
               $str = file_get_contents($url);
               preg_match_all('#<'.$tag.'>(.+?)</'.$tag.'>#su', $str, $res);
               return $res;
            }
    }


$parser = new Parser();
$result = $parser->process('https://www.bbc.com/', 'h3');
echo '<pre>';
print_r($result);